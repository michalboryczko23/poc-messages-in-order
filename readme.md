## How to run:

    docker-compose up -d
    docker-compose exec -u application php composer install
    docker-compose exec -u application bin/cosnole messenger:consume input_message
    docker-compose exec -u application bin/cosnole messenger:consume handled_notification
