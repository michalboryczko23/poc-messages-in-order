<?php

namespace App\Infrastructure\Queue;

use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;

class MessageSerializer implements SerializerInterface
{
    public function __construct(private SerializerInterface $serializer)
    {
    }

    public function decode(array $encodedEnvelope): Envelope
    {
        if (isset($encodedEnvelope['headers']['type']) && $encodedEnvelope['headers']['type'] === 'handled_notification') {
            $encodedEnvelope['headers']['type'] = 'App\Infrastructure\UI\Queue\Message\HandledNotificationMessage';
        } elseif (!isset($encodedEnvelope['headers']['type'])) {
            $encodedEnvelope['headers']['type'] = 'App\Infrastructure\UI\Queue\Message\InputMessage';
        }

        return $this->serializer->decode($encodedEnvelope);
    }

    public function encode(Envelope $envelope): array
    {
        return $this->serializer->encode($envelope);
    }
}
