<?php

namespace App\Infrastructure\UI\Queue\MessageHandler;

use App\Infrastructure\UI\Queue\Message\InputMessage;
use App\Infrastructure\UI\Queue\Message\OutputMessage;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class InputMessageHandler implements MessageHandlerInterface
{
    public function __construct(
        private MessageBusInterface $messageBus,
        private \Redis $redis
    ) {
    }

    public function __invoke(InputMessage $message)
    {
        if ($this->redis->hGet('currentlyProcessedHotels', $message->getChannelHotelCode())) {
            throw new \Exception('currently processing');
        }
        $this->redis->hSet('currentlyProcessedHotels', $message->getChannelHotelCode(), true);

        $this->redis->hSet(
            sprintf('messagesOrder_%s', $message->getChannelHotelCode()),
            $message->getId(),
            json_encode([
                'message_id' => $message->getId(),
                'published_at' => $message->getPublishedAt()
            ])
        );

        $hotelMessages = $this->redis->hGetAll(sprintf('messagesOrder_%s', $message->getChannelHotelCode()));
        $decodedHotelMessages = [];
        foreach ($hotelMessages as $messageId => $hotelMessage) {
            $decodedHotelMessages[$messageId] = json_decode($hotelMessage, true);
        }

        uasort(
            $decodedHotelMessages,
            function (array $a, array $b) {
                return $a['published_at'] <=> $b['published_at'];
            }
        );

        if (array_values($decodedHotelMessages)[0]['message_id'] !== $message->getId()) {
            throw new \Exception('Is not oldest');
        }

        $this->messageBus->dispatch(OutputMessage::createFromInputMessage($message));
    }
}
