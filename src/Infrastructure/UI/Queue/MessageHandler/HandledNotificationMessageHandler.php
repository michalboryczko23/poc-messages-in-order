<?php

namespace App\Infrastructure\UI\Queue\MessageHandler;

use App\Infrastructure\UI\Queue\Message\HandledNotificationMessage;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class HandledNotificationMessageHandler implements MessageHandlerInterface
{
    public function __construct(private \Redis $redis)
    {
    }

    public function __invoke(HandledNotificationMessage $message)
    {
        $this->redis->hSet('currentlyProcessedHotels', $message->getChannelHotelCode(), false);
        $this->redis->hDel(
            sprintf('messagesOrder_%s', $message->getChannelHotelCode()),
            $message->getId()
        );
    }
}
