<?php

namespace App\Infrastructure\UI\Queue\Message;

class HandledNotificationMessage
{
    public function __construct(
        private string $id,
        private int $channelHotelCode
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getChannelHotelCode(): int
    {
        return $this->channelHotelCode;
    }
}
