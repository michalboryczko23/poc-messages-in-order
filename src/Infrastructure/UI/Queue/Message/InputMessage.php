<?php

namespace App\Infrastructure\UI\Queue\Message;

class InputMessage
{
    public function __construct(
        private string $id,
        private int    $publishedAt,
        private int    $channelHotelCode,
        private string $input,
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getPublishedAt(): int
    {
        return $this->publishedAt;
    }

    public function getChannelHotelCode(): int
    {
        return $this->channelHotelCode;
    }

    public function getInput(): string
    {
        return $this->input;
    }
}
