<?php

namespace App\Infrastructure\UI\Queue\Message;

class OutputMessage
{
    public function __construct(
        private string $id,
        private int    $publishedAt,
        private int    $channelHotelCode,
        private string $input,
    ) {
    }

    public static function createFromInputMessage(InputMessage $inputMessage): self
    {
        return new self(
            $inputMessage->getId(),
            $inputMessage->getPublishedAt(),
            $inputMessage->getChannelHotelCode(),
            $inputMessage->getInput()
        );
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getPublishedAt(): int
    {
        return $this->publishedAt;
    }

    public function getChannelHotelCode(): int
    {
        return $this->channelHotelCode;
    }

    public function getInput(): string
    {
        return $this->input;
    }
}
